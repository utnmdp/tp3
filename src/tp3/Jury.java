package tp3;

import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Jury implements Observer {

    /**
     * @Override
     * Determina quien gana el juego
     * @param observable - El dealer del juego
     * @param o - Datos que envia el dealer
     */
    public synchronized void update(Observable observable, Object o) {
        Player winner = null;
        HashMap<String, Object> map = (HashMap<String, Object>) o;
        List<Player> players = (ArrayList<Player>) map.get("players");

        // Si el dealer notifica que se queda sin cartas, busca el ganador con mas puntos
        if ((int) map.get("deckSize") < 1) {
            for (Player player : players) {
                if (winner == null) {
                    winner = player;
                } else {
                    if (player.getScore() > winner.getScore()) {
                        winner = player;
                    }
                }
            }

            System.out.println("El ganador es " + winner.getName());
            this.saveWinner(winner);
        }
    }

    /**
     * Guarda la cantidad de puntos del ganador de la partida en la base de datos
     * @param winner - Jugador que gano la partida
     */
    private void saveWinner(Player winner) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/tp3?user=root&password=11020101&useSSL=false");
            PreparedStatement ps = conn.prepareStatement("INSERT INTO games(winner_name, points) values (?, ?)");

            ps.setString(1, winner.getName());
            ps.setInt(2, winner.getScore());

            boolean result = ps.execute();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
