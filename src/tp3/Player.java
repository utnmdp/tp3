package tp3;

import java.util.Observable;

public class Player extends Observable implements Runnable {

    private Deck hand;
    private String name;
    private boolean play;
    private Dealer dealer;

    Player(String name, Table table, Dealer dealer) {
        this.hand = new Deck();
        this.name = name;
        this.play = true;
        this.dealer = dealer;

        this.addObserver(table);
    }

    @Override
    public void run() {
        while (this.play) {
            Card card = this.dealer.deal();

            if (card == null) {
                this.play = false;
            } else {
                try {
                    this.addCard(card);

                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Deck getHand() {
        return hand;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        int score = 0;

        for (Card card : this.hand.getCards()) {
            score += card.getValue();
        }

        return score;
    }

    private void addCard(Card card) {
        this.hand.addCard(card);

        this.setChanged();
        this.notifyObservers(card);
    }
}
