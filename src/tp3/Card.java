package tp3;

public class Card {

    private CardSuit suit;
    private int value;

    Card(CardSuit suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return this.value + " of " + this.suit;
    }

}
