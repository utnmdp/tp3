package tp3;

import java.util.Stack;
import java.util.Collections;

public class Deck {

    private Stack<Card> cards;

    Deck() {
        this.cards = new Stack<>();
    }

    Deck(int numberOfCards) {
        this.cards = new Stack<>();

        for (CardSuit suit : CardSuit.values()) {
            for (int i = 1; i <= numberOfCards; i++) {
                this.cards.push(new Card(suit, i));
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }

    public void addCard(Card card) {
        this.cards.push(card);
    }

    public Card getTopCard() {
        return this.cards.pop();
    }

    public Stack<Card> getCards() {
        return this.cards;
    }
}
