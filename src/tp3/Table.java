package tp3;

import java.util.*;

public class Table implements Observer {

    /**
     * @Override
     * Actualiza la "interfaz" grafica cuando un jugador recibe una nueva carta
     * @param observable - El jugador que recibio una nueva carta
     * @param o - La nueva carta del jugador
     */
    public synchronized void update(Observable observable, Object o) {
        Card newCard = (Card) o;
        Player player = (Player) observable;

        System.out.println(player.getName() + " score: " + player.getScore() + " cartas: ");

        for (Card card : player.getHand().getCards()) {
            if (card.equals(newCard)) {
                System.out.println("\t" + card.toString() + " (nueva)");
            } else {
                System.out.println("\t" + card.toString());
            }
        }
    }
}

