package tp3;

public class Main {

    public static void main(String[] args) {
        // La mesa de juego donde se va a jugar la partida (seria la UI)
        Table table = new Table();

        // El jugado que determina el ganador del juego
        Jury jury = new Jury();

        // El dealer que reparte las cartas y determina con cuantas jugar
        Dealer dealer = new Dealer("OsquiDelear", jury, 3);

        // Todos los jugadores de la partida
        Thread[] players = new Thread[4];

        for (int i = 0; i < 4; i++) {
            Player player = new Player("Jugador" + i, table, dealer);

            // El delealer agrega al jugador al juego
            dealer.addPlayer(player);

            players[i] = new Thread(player);

            // El jugador emmpieza a jugar
            players[i].start();
        }
    }

}
