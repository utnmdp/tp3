package tp3;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Observable;

public class Dealer extends Observable {

    private Deck deck;
    private String name;
    private List<Player> players;

    Dealer(String name, Jury jury, int numberOfCards) {
        this.name = name;
        this.players = new ArrayList<>();
        this.deck = new Deck(numberOfCards);

        this.addObserver(jury);

        // Mezcla el mazo de cartas para que no quede ordenado
        this.deck.shuffle();
    }

    public synchronized Card deal() {
        if (this.deck.getCards().size() >= 1) {
            return this.giveCard();
        }

        return null;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public String getName() {
        return name;
    }

    public Deck getDeck() {
        return deck;
    }

    private Card giveCard() {
        Card card = this.deck.getTopCard();

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("players", this.players);
        map.put("deckSize", this.deck.getCards().size());

        this.setChanged();
        this.notifyObservers(map);

        return card;
    }

}
