# Laboratorio V

##Trabajo practico N°3: Juego de cartas

### Persistencia utilizada y porque se eligió
No entiendi esto.. preguntar.

Como motor de base de datos se elgió MYSQL (porque sí) y se utilizó el driver
de mysql para java para poder conectarse a la base de datos y hacer consultas.
Tecnicamente se tendria que haber implementado el patron DAO ó Active Records, 
pero como el punto principal del trabajo práctico era utilizar hilos y el patron observer opté
por dejar más simple el acceso a datos.

###Explicación del juego
Seguí todas las consignas del trabajo práctico, solo que cambié algunas cosas.
En este juego hay 4 jugadores, un repartidor y un jurado.
Antes de empezar la partida, el jurado determina con cuantas cartas jugar y mezcla el mazo.
Luego agrega a los jugadores a la partida, una vez que los jugadores fueron agregados tratan 
de pedirte una carta al repartidor.

El repartidor le da una carta al primer jugador que se lo pidió, y este al recibir la carta tiene que esperar un tiempo para poder pedir otra.
Este proceso se repite constantemente hasta que el repartidor se quede sin cartas. Cuando el repartidor se queda sin cartas
le notifica al jurado, para que este evalue quien es el jugador gandor.

Para determinar que jugador ganó la partida, el jurado suma el valor de cada carta de cada jugador.

###Realizar driagrama UML y adjuntarlo
Lo pedis, lo tenes:
![UML](https://bytebucket.org/utnmdp/tp3/raw/a5a5a9f2846bf426e9894a417ca29f998188727b/UML.png)